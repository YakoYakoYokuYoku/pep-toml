TOML PEP Draft
==============

This repository holds a draft of a PEP_ for TOML_ support in the standard library of Python.

See the `PEP source text </pep-toml.txt>`_ or the `rendered text </pep-toml.html>`_ for details.

.. _PEP: https://www.python.org/dev/peps/
.. _TOML: https://toml.io/en/
